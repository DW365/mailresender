﻿using System;
using System.Threading;
using System.Text;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;
using System.Net.Mail;
using Limilabs.Mail;
using Limilabs.Client.IMAP;


namespace MailResender
{
    class Core
    {
        Imap imap;
        SmtpClient mySmtpClient;
        public Core()
        {
            Globals.LoadData();
            Connect();
            mySmtpClient = new SmtpClient("smtp.mail.ru");
            mySmtpClient.EnableSsl = true;
            mySmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            mySmtpClient.Credentials = new NetworkCredential(Globals.LOGIN, Globals.PASSWORD);
        }
        private void Connect()
        {
            imap = new Imap();
            try
            {
                imap.Connect("imap.mail.ru");
                imap.Login(Globals.LOGIN, Globals.PASSWORD);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Авторизация почты прошла успешно");
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ошибка подключения к почте \n{0}", e.Message);
            }
        }
        public void Start()
        {
            for (;;)
            {
                CheckMail();
                System.Threading.Thread.Sleep(Globals.DELAY);
            } 
        }
        private void Send(string text)
        {
            try
            {
                SendMailMessage(Globals.MAIL, Globals.LOGIN, "Новый прогноз", text, true);
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.WriteLine("[{0:H:mm:ss}] Успешно переслано", DateTime.Now);
            }
            catch(Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ошибка отправки \n{0}", e.Message);
                System.Threading.Thread.Sleep(10000);
                Send(text);
            }
        }
        private void SendMailMessage(string SendTo, string From, string Subject, string Body, bool IsBodyHtml)
        {

            MailMessage htmlMessage;

            htmlMessage = new MailMessage(Globals.LOGIN, Globals.MAIL, Subject, Body);
            htmlMessage.IsBodyHtml = IsBodyHtml;

            mySmtpClient.Send(htmlMessage);
        }
        private void CheckMail()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("[{0:H:mm:ss}] Читаю почту", DateTime.Now);
            try
            {
                imap.SelectInbox();
                List<long> uids = imap.Search(Flag.Unseen);
                if (uids.Count >= 1)
                {
                    var eml = imap.GetMessageByUID(uids[0]);
                    IMail mail = new MailBuilder().CreateFromEml(eml);
                    Console.ForegroundColor = ConsoleColor.DarkBlue;
                    Console.WriteLine("[SENDER] {0}", mail.From[0].Address);
                    if (mail.From[0].Address == Globals.SENDER)
                    {
                        //Console.WriteLine("[MESSAGE RAW]\n{0}", mail.HtmlData.Text);
                        string msg = mail.Html;
                        Regex reg = new Regex(@"<table.*>(.*)</table>", RegexOptions.Singleline);
                        MatchCollection matches = reg.Matches(msg);
                        if (matches.Count >= 1)
                            Send(matches[0].Value);
                        else
                            throw new Exception("Таблица не выделена из текста, обратитесь к разработчику");
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Письмо от неизвесного адресата");
                    }
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine("Новых писем нет");
                }
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ошибка чтения почты \n{0}", e.Message);
                imap.Close();
                Connect();
            }
        }
    }
}
