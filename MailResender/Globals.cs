﻿using System;

namespace MailResender
{
    static class Globals
    {
        public static string LOGIN;
        public static string PASSWORD;
        public static string MAIL;
        public static string SENDER;
        public static int DELAY;

        public static void LoadData(string filename = "data.cfg")
        {
            try
            {
                int counter = 0; string line;
                System.IO.StreamReader file = new System.IO.StreamReader(filename);
                while ((line = file.ReadLine()) != null)
                {
                    if (counter == 0) LOGIN = line;
                    if (counter == 1) PASSWORD = line;
                    if (counter == 2) SENDER = line;
                    if (counter == 3) MAIL = line;
                    if (counter == 4) DELAY = Convert.ToInt32(line);
                    counter++;
                }
                file.Close();
                if (counter != 5)
                    throw new Exception("Неверный формат файла");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Настройки успешно загружены");
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ошибка чтения настроек \n{0}", e.Message);
                Console.ReadKey();
                System.Environment.Exit(2);
            }
        }
    }
}
