﻿namespace MailResender
{
    class Program
    {
        static void Main(string[] args)
        {
            var core = new Core();
            core.Start();
        }
    }
}
